package com.proof.sura.Builders.repositories;

import com.proof.sura.Builders.entities.Materials;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface IMaterialsRepository extends CrudRepository<Materials, Integer> {
    @Query("SELECT m.id FROM Materials m WHERE m.acronym=:acronym AND m.quantity>=:quantity")
    Integer quantityAvailable(@Param("acronym") String acronym, @Param("quantity") Integer quantity);

    @Transactional
    @Modifying
    @Query("Update Materials m SET m.quantity=m.quantity-:quantity WHERE m.acronym=:acronym")
    void modifyInventory(@Param("quantity") Integer quantity, @Param("acronym") String acronym);
}

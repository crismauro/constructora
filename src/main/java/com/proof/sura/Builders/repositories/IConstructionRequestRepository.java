package com.proof.sura.Builders.repositories;

import com.proof.sura.Builders.entities.ConstructionRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IConstructionRequestRepository extends CrudRepository<ConstructionRequest, Integer> {
}

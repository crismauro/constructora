package com.proof.sura.Builders.repositories;

import com.proof.sura.Builders.entities.ConstructionOrder;
import com.proof.sura.Builders.entities.ConstructionRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IConstructionOrderRepository extends CrudRepository<ConstructionOrder, Integer> {
    @Query("SELECT o.id FROM ConstructionOrder o WHERE o.constructionRequest.coordinate=:coordinate")
    Integer coordinateExists(@Param("coordinate") String coordinate);

    @Query("SELECT o FROM ConstructionOrder o WHERE o.status='Pendiente' AND \n" +
            " (SELECT COUNT(c.id) FROM ConstructionOrder c WHERE c.status='En progreso')=0 ORDER BY o.id ASC")
    List<ConstructionOrder> pendingOrder();

    @Query("SELECT o.constructionRequest FROM ConstructionOrder o WHERE o.id=:id")
    ConstructionRequest requestDate(@Param("id") Integer id);

    @Query("SELECT o FROM ConstructionOrder o WHERE o.status='En progreso'")
    List<ConstructionOrder> constructionFinished();

    @Query("SELECT o.status, COUNT(o.id) FROM ConstructionOrder o GROUP BY o.status")
    List<Object[]> report();

    @Query("SELECT o.endDate FROM ConstructionOrder o WHERE o.status='En progreso'")
    Date endOfProject();
}

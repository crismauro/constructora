package com.proof.sura.Builders.repositories;

import com.proof.sura.Builders.entities.ConstructionType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IConstructionTypeRepository extends CrudRepository<ConstructionType, Integer> {
}

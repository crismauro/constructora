package com.proof.sura.Builders.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConstructionRequestDTO {
    private Integer id;

    @NotNull
    private ConstructionTypeDTO constructionType;

    @NotNull
    @Length(max = 30)
    private String coordinate;

    @NotNull
    private Date requestDate;
}


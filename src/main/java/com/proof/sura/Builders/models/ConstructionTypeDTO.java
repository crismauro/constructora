package com.proof.sura.Builders.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConstructionTypeDTO {
    private Integer id;

    @NotNull
    @Length(max = 20)
    private String name;

    @NotNull
    private Integer ce;

    @NotNull
    private Integer gr;

    @NotNull
    private Integer ar;

    @NotNull
    private Integer ma;

    @NotNull
    private Integer ad;

    @NotNull
    private Integer days;
}

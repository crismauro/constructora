package com.proof.sura.Builders.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MaterialsDTO {
    private Integer id;

    @NotNull
    @Length(max = 10)
    private String name;

    @NotNull
    @Length(max = 2)
    private String acronym;

    private Integer quantity;
}

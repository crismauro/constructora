package com.proof.sura.Builders.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SaveConstructionOrderDTO {
    private Integer id;

    @NotNull
    private Integer constructionRequestId;

    @NotNull
    @Length(max = 15)
    private String status;

    private Date startDate;

    private Date endDate;
}


package com.proof.sura.Builders.services;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;

@Service
public class ReportImpl implements IReport {
    @Autowired
    private IConstructionOrder iConstructionOrder;

    @SneakyThrows
    @Override
    public void generateReport() {
        FileWriter fileWriter = null;
        fileWriter = new FileWriter("Informe.txt");
        FileWriter finalFileWriter = fileWriter;
        finalFileWriter.write("|--------------------------|\n");
        finalFileWriter.write("|   INFORME CONSTRUCIONES  |\n");
        finalFileWriter.write("|--------------------------|\n");
        iConstructionOrder.report().stream().forEach(report -> {
            try {
                String status;
                if (String.valueOf(report[0]).length() == 10) {
                    status = String.format("|%s ", String.valueOf(report[0]));
                } else if (String.valueOf(report[0]).length() == 9) {
                    status = String.format("|%s  ", String.valueOf(report[0]));
                } else {
                    status = String.format("|%s", String.valueOf(report[0]));
                }

                finalFileWriter.write(String.format("%s   |          %s|\n", status, String.valueOf(report[1])));
                finalFileWriter.write("|--------------------------|\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        finalFileWriter.write(String.format("\n\n\n La finalización del proyecto es en %s", iConstructionOrder.endOfProject()));

        finalFileWriter.close();
        fileWriter.close();
    }
}

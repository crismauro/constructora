package com.proof.sura.Builders.services;

import com.proof.sura.Builders.entities.Materials;
import com.proof.sura.Builders.models.MaterialsDTO;
import com.proof.sura.Builders.models.SaveMaterialsDTO;
import com.proof.sura.Builders.repositories.IMaterialsRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MaterialsImpl implements IMaterials {
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private IMaterialsRepository iMaterialsRepository;

    @Override
    public MaterialsDTO save(SaveMaterialsDTO saveMaterialsDTO) {
        Materials materials = Materials.builder()
                .id(saveMaterialsDTO.getId())
                .name(saveMaterialsDTO.getName())
                .acronym(saveMaterialsDTO.getAcronym())
                .quantity(saveMaterialsDTO.getQuantity())
                .build();
        materials = iMaterialsRepository.save(materials);

        return modelMapper.map(materials, MaterialsDTO.class);
    }

    @Override
    public Integer quantityAvailable(String acronym, Integer quantity) {
        return iMaterialsRepository.quantityAvailable(acronym, quantity);
    }

    @Override
    public void modifyInventory(Integer quantity, String acronym) {
        iMaterialsRepository.modifyInventory(quantity, acronym);
    }
}

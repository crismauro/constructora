package com.proof.sura.Builders.services;

import com.proof.sura.Builders.entities.ConstructionOrder;
import com.proof.sura.Builders.entities.ConstructionRequest;
import com.proof.sura.Builders.models.ConstructionOrderDTO;
import com.proof.sura.Builders.models.SaveConstructionOrderDTO;
import com.proof.sura.Builders.repositories.IConstructionOrderRepository;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConstructionOrderImpl implements IConstructionOrder {
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private IConstructionOrderRepository iConstructionOrderRepository;

    @Autowired
    private IConstructionRequest iConstructionRequest;

    @Override
    public void generateOrder(SaveConstructionOrderDTO saveConstructionOrderDTO) {
        ConstructionOrder constructionOrder = ConstructionOrder.builder()
                .id(saveConstructionOrderDTO.getId())
                .constructionRequest(new ConstructionRequest(saveConstructionOrderDTO.getConstructionRequestId()))
                .status(saveConstructionOrderDTO.getStatus())
                .startDate(saveConstructionOrderDTO.getStartDate())
                .endDate(saveConstructionOrderDTO.getEndDate())
                .build();

        iConstructionOrderRepository.save(constructionOrder);
    }

    @Override
    public Integer coordinateExists(String coordinate) {
        return iConstructionOrderRepository.coordinateExists(coordinate);
    }

    @Override
    public ConstructionOrderDTO pendingOrder() {
        List<ConstructionOrder> constructionOrders = iConstructionOrderRepository.pendingOrder();
        if (constructionOrders.size() > 0)
            return modelMapper.map(constructionOrders.get(0), ConstructionOrderDTO.class);

        return null;
    }

    @SneakyThrows
    @Override
    public void orderInProgress(Integer id) {
        Calendar date = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        ConstructionRequest constructionRequest = iConstructionOrderRepository.requestDate(id);

        if (new SimpleDateFormat("yyyy-MM-dd").format(constructionRequest.getRequestDate()).equals(new SimpleDateFormat("yyyy-MM-dd").format(date.getTime()))) {
            startDate.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH) + 1);
            endDate.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH) + (constructionRequest.getConstructionType().getDays() + 2));
        } else {
            startDate.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH) + 1);
            endDate.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH) + (constructionRequest.getConstructionType().getDays() + 1));
        }

        iConstructionOrderRepository.save(ConstructionOrder.builder()
                .id(id)
                .constructionRequest(constructionRequest)
                .status("En progreso")
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .build());
    }

    @Override
    public ConstructionOrderDTO constructionFinished() {
        List<ConstructionOrder> constructionOrders = iConstructionOrderRepository.constructionFinished().stream()
                .filter(order -> new SimpleDateFormat("yyyy-MM-dd").format(order.getEndDate()).equals(new SimpleDateFormat("yyyy-MM-dd").format(new Date())))
                .collect(Collectors.toList());

        if (constructionOrders.size() > 0)
            return modelMapper.map(constructionOrders.get(0), ConstructionOrderDTO.class);

        return null;
    }

    @Override
    public void finalizeOrder(Integer id) {
        ConstructionOrder constructionOrder = iConstructionOrderRepository.findById(id).get();

        iConstructionOrderRepository.save(ConstructionOrder.builder()
                .id(id)
                .constructionRequest(constructionOrder.getConstructionRequest())
                .status("Finalizado")
                .startDate(constructionOrder.getStartDate())
                .endDate(constructionOrder.getEndDate())
                .build());
    }

    @Override
    public List<Object[]> report() {
        return iConstructionOrderRepository.report();
    }

    @Override
    public String endOfProject() {
        return iConstructionOrderRepository.endOfProject().toString();
    }
}

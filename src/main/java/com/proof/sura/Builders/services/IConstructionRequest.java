package com.proof.sura.Builders.services;

import com.proof.sura.Builders.models.SaveConstructionRequestDTO;

public interface IConstructionRequest {
    void generateRequest(SaveConstructionRequestDTO saveConstructionRequestDTO);
}

package com.proof.sura.Builders.services;

import com.proof.sura.Builders.entities.ConstructionType;
import com.proof.sura.Builders.models.ConstructionTypeDTO;
import com.proof.sura.Builders.models.SaveConstructionTypeDTO;
import com.proof.sura.Builders.repositories.IConstructionTypeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConstructionTypeImpl implements IConstructionType {
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private IConstructionTypeRepository iConstructionTypeRepository;

    @Override
    public ConstructionTypeDTO save(SaveConstructionTypeDTO saveConstructionTypeDTO) {
        ConstructionType constructionType = ConstructionType.builder()
                .id(saveConstructionTypeDTO.getId())
                .name(saveConstructionTypeDTO.getName())
                .ce(saveConstructionTypeDTO.getCe())
                .gr(saveConstructionTypeDTO.getGr())
                .ar(saveConstructionTypeDTO.getAr())
                .ma(saveConstructionTypeDTO.getMa())
                .ad(saveConstructionTypeDTO.getAd())
                .days(saveConstructionTypeDTO.getDays())
                .build();

        constructionType = iConstructionTypeRepository.save(constructionType);

        return modelMapper.map(constructionType, ConstructionTypeDTO.class);
    }

    @Override
    public ConstructionTypeDTO findById(Integer id) {
        return modelMapper.map(iConstructionTypeRepository.findById(id).orElse(null), ConstructionTypeDTO.class);
    }
}

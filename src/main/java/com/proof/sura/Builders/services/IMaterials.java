package com.proof.sura.Builders.services;

import com.proof.sura.Builders.models.MaterialsDTO;
import com.proof.sura.Builders.models.SaveMaterialsDTO;

public interface IMaterials {
    MaterialsDTO save(SaveMaterialsDTO saveMaterialsDTO);

    Integer quantityAvailable(String acronym, Integer quantity);

    void modifyInventory(Integer quantity, String acronym);
}

package com.proof.sura.Builders.services;

import com.proof.sura.Builders.entities.ConstructionRequest;
import com.proof.sura.Builders.entities.ConstructionType;
import com.proof.sura.Builders.models.*;
import com.proof.sura.Builders.repositories.IConstructionRequestRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConstructionRequestImpl implements IConstructionRequest {
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private IMaterials iMaterials;

    @Autowired
    private IConstructionType iConstructionType;

    @Autowired
    private IConstructionRequestRepository iConstructionRequestRepository;

    @Autowired
    private IConstructionOrder iConstructionOrder;

    private static List<ConstructionRequest> constructionRequests = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void generateRequest(SaveConstructionRequestDTO saveConstructionRequestDTO) {
        ConstructionTypeDTO constructionTypeDTO = iConstructionType.findById(saveConstructionRequestDTO.getConstructionTypeId());

        iMaterials.modifyInventory(constructionTypeDTO.getCe(), "Ce");
        iMaterials.modifyInventory(constructionTypeDTO.getGr(), "Gr");
        iMaterials.modifyInventory(constructionTypeDTO.getAr(), "Ar");
        iMaterials.modifyInventory(constructionTypeDTO.getMa(), "Ma");
        iMaterials.modifyInventory(constructionTypeDTO.getAd(), "Ad");

        ConstructionRequest constructionRequest = ConstructionRequest.builder()
                .id(saveConstructionRequestDTO.getId())
                .constructionType(new ConstructionType(saveConstructionRequestDTO.getConstructionTypeId()))
                .coordinate(saveConstructionRequestDTO.getCoordinate())
                .requestDate(new Date())
                .build();

        constructionRequest = iConstructionRequestRepository.save(constructionRequest);

        constructionRequests.add(constructionRequest);
    }

    @Scheduled(fixedDelay = 1000 * 10)
    public void scheduleFixedDelayTask() {
        constructionRequests.removeAll(constructionRequests.stream()
                .map(request -> {
                    iConstructionOrder.generateOrder(SaveConstructionOrderDTO.builder()
                            .constructionRequestId(request.getId())
                            .status("Pendiente")
                            .build());

                    return request;
                })
                .collect(Collectors.toList())
        );
    }
}

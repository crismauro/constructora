package com.proof.sura.Builders.services;

import com.proof.sura.Builders.models.ConstructionTypeDTO;
import com.proof.sura.Builders.models.SaveConstructionTypeDTO;

public interface IConstructionType {
    ConstructionTypeDTO save(SaveConstructionTypeDTO saveConstructionTypeDTO);

    ConstructionTypeDTO findById(Integer id);
}

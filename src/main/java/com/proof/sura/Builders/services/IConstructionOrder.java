package com.proof.sura.Builders.services;

import com.proof.sura.Builders.models.ConstructionOrderDTO;
import com.proof.sura.Builders.models.SaveConstructionOrderDTO;

import java.util.List;

public interface IConstructionOrder {
    void generateOrder(SaveConstructionOrderDTO saveConstructionOrderDTO);

    Integer coordinateExists(String coordinate);

    ConstructionOrderDTO pendingOrder();

    void orderInProgress(Integer id);

    ConstructionOrderDTO constructionFinished();

    void finalizeOrder(Integer id);

    List<Object[]> report();

    String endOfProject();
}

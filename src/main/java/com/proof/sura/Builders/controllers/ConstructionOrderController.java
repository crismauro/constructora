package com.proof.sura.Builders.controllers;

import com.proof.sura.Builders.models.ConstructionOrderDTO;
import com.proof.sura.Builders.services.IConstructionOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping(path = "/rest/constructionOrders")
public class ConstructionOrderController {
    @Autowired
    private IConstructionOrder iConstructionOrder;

    @GetMapping(path = "/pendingOrder")
    public ResponseEntity<?> pendingOrder() {
        if (Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) >= 1 &&
                Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) <= 11) {
            return new ResponseEntity<ConstructionOrderDTO>(iConstructionOrder.pendingOrder(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Este proceso se realiza en las horas de la mañana", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/orderInProgress/{id}")
    public ResponseEntity orderInProgress(@PathVariable Integer id) {
        if (Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) >= 1 &&
                Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) <= 11) {
            iConstructionOrder.orderInProgress(id);
            return new ResponseEntity("Orden puesta en progreso", HttpStatus.OK);
        } else {
            return new ResponseEntity("Este proceso se realiza en las horas de la mañana", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/constructionFinished")
    public ResponseEntity<?> constructionFinished() {
        if (Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) >= 18 &&
                Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) <= 23) {
            return new ResponseEntity<ConstructionOrderDTO>(iConstructionOrder.constructionFinished(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Este proceso se realiza en las horas de la noche", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/finalizeOrder/{id}")
    public ResponseEntity finalizeOrder(@PathVariable Integer id) {
        if (Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) >= 18 &&
                Integer.valueOf(new SimpleDateFormat("HH").format(new Date())) <= 23) {
            iConstructionOrder.finalizeOrder(id);
            return new ResponseEntity("Orden finalizada", HttpStatus.OK);
        } else {
            return new ResponseEntity("Este proceso se realiza en las horas de la noche", HttpStatus.BAD_REQUEST);
        }
    }
}

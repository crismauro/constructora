package com.proof.sura.Builders.controllers;

import com.proof.sura.Builders.services.IReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/rest/reports")
public class ReportController {
    @Autowired
    private IReport iReport;

    @GetMapping(path = "/generateReport")
    public ResponseEntity generateReport() {
        iReport.generateReport();
        return new ResponseEntity("Resumen generado", HttpStatus.OK);
    }
}

package com.proof.sura.Builders.controllers;

import com.proof.sura.Builders.models.ConstructionTypeDTO;
import com.proof.sura.Builders.models.SaveConstructionTypeDTO;
import com.proof.sura.Builders.services.IConstructionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/rest/constructionTypes")
public class ConstructionTypeController {
    @Autowired
    private IConstructionType iConstructionType;

    @PostMapping
    public ConstructionTypeDTO save(@RequestBody @Validated SaveConstructionTypeDTO saveConstructionTypeDTO) {
        return iConstructionType.save(saveConstructionTypeDTO);
    }
}

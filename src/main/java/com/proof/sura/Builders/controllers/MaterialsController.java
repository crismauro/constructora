package com.proof.sura.Builders.controllers;

import com.proof.sura.Builders.models.MaterialsDTO;
import com.proof.sura.Builders.models.SaveMaterialsDTO;
import com.proof.sura.Builders.services.IMaterials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/rest/materials")
public class MaterialsController {
    @Autowired
    private IMaterials iMaterials;

    @PostMapping
    public MaterialsDTO save(@RequestBody @Validated SaveMaterialsDTO saveMaterialsDTO) {
        return iMaterials.save(saveMaterialsDTO);
    }
}

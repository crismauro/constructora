package com.proof.sura.Builders.controllers;

import com.proof.sura.Builders.models.ConstructionTypeDTO;
import com.proof.sura.Builders.models.SaveConstructionRequestDTO;
import com.proof.sura.Builders.services.IConstructionOrder;
import com.proof.sura.Builders.services.IConstructionRequest;
import com.proof.sura.Builders.services.IConstructionType;
import com.proof.sura.Builders.services.IMaterials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/rest/constructionRequests")
public class ConstructionRequestController {
    @Autowired
    private IConstructionRequest iConstructionRequest;

    @Autowired
    private IMaterials iMaterials;

    @Autowired
    private IConstructionOrder iConstructionOrder;

    @Autowired
    private IConstructionType iConstructionType;

    @PostMapping
    public ResponseEntity save(@RequestBody @Validated SaveConstructionRequestDTO saveConstructionRequestDTO) {
        ConstructionTypeDTO constructionTypeDTO = iConstructionType.findById(saveConstructionRequestDTO.getConstructionTypeId());

        if (iMaterials.quantityAvailable("Ce", constructionTypeDTO.getCe()) == null)
            return new ResponseEntity("No se cuenta con el cemento suficiente", HttpStatus.BAD_REQUEST);
        if (iMaterials.quantityAvailable("Gr", constructionTypeDTO.getGr()) == null)
            return new ResponseEntity("No se cuenta con la grava suficiente", HttpStatus.BAD_REQUEST);
        if (iMaterials.quantityAvailable("Ar", constructionTypeDTO.getAr()) == null)
            return new ResponseEntity("No se cuenta con la arena suficiente", HttpStatus.BAD_REQUEST);
        if (iMaterials.quantityAvailable("Ma", constructionTypeDTO.getMa()) == null)
            return new ResponseEntity("No se cuenta con la madera suficiente", HttpStatus.BAD_REQUEST);
        if (iMaterials.quantityAvailable("Ad", constructionTypeDTO.getAd()) == null)
            return new ResponseEntity("No se cuenta con el adobe suficiente", HttpStatus.BAD_REQUEST);

        if (iConstructionOrder.coordinateExists(saveConstructionRequestDTO.getCoordinate()) != null)
            return new ResponseEntity("En la coordenadas ya hay una orden", HttpStatus.BAD_REQUEST);

        iConstructionRequest.generateRequest(saveConstructionRequestDTO);

        return new ResponseEntity("Solicitud realizada con éxito", HttpStatus.OK);
    }
}

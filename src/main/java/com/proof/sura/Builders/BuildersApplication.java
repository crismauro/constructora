package com.proof.sura.Builders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class BuildersApplication {
	public static void main(String[] args) {
		SpringApplication.run(BuildersApplication.class, args);
	}

}

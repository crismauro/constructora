package com.proof.sura.Builders.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "b_construction_type")
public class ConstructionType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Column(name = "name", length = 20)
    private String name;

    @NotNull
    @Column(name = "ce")
    private Integer ce;

    @NotNull
    @Column(name = "gr")
    private Integer gr;

    @NotNull
    @Column(name = "ar")
    private Integer ar;

    @NotNull
    @Column(name = "ma")
    private Integer ma;

    @NotNull
    @Column(name = "ad")
    private Integer ad;

    @NotNull
    @Column(name = "days")
    private Integer days;

    public ConstructionType(Integer id) {
        this.id = id;
    }
}

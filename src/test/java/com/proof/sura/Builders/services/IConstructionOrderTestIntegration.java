package com.proof.sura.Builders.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class IConstructionOrderTestIntegration {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private IConstructionOrder iConstructionOrder;

    @Test
    public void pendingOrder() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/rest/constructionOrders/pendingOrder")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void orderInProgress() throws Exception {
        Integer id = iConstructionOrder.pendingOrder().getId();
        if (id != null)
            mockMvc.perform(MockMvcRequestBuilders
                    .get(String.format("/rest/constructionOrders/orderInProgress/%d", id))
                    .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk());
    }

    @Test
    public void constructionFinished() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/rest/constructionOrders/constructionFinished")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void finalizeOrder() throws Exception {
        Integer id = iConstructionOrder.constructionFinished().getId();
        if (id != null)
            mockMvc.perform(MockMvcRequestBuilders
                    .get(String.format("/rest/constructionOrders/finalizeOrder/%d", id))
                    .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk());
    }
}

package com.proof.sura.Builders.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.proof.sura.Builders.auxiliaryTest.AuxiliaryConstructionRequestTest;
import com.proof.sura.Builders.models.SaveConstructionRequestDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class IConstructionRequestTestIntegration {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void generateRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/rest/constructionRequests")
                .content(objectMapper.writeValueAsString(AuxiliaryConstructionRequestTest.getConstructionRequestDTO()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}

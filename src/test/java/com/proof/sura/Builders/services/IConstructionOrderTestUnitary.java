package com.proof.sura.Builders.services;

import com.proof.sura.Builders.models.ConstructionOrderDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IConstructionOrderTestUnitary {
    @Autowired
    private IConstructionOrder iConstructionOrder;

    @Test
    public void coordinateExists() {
        Integer result = iConstructionOrder.coordinateExists("N 06 22 45, W 75 23 11");
        assertEquals(null, result);
    }

    @Test
    public void pendingOrder() {
        ConstructionOrderDTO constructionOrderDTO = iConstructionOrder.pendingOrder();
        assertNotEquals(null, constructionOrderDTO);
    }

    @Test
    public void constructionFinished() {
        ConstructionOrderDTO constructionOrderDTO = iConstructionOrder.constructionFinished();
        assertNotEquals(null, constructionOrderDTO);
    }

    @Test
    public void report() {
        List<Object[]> report = iConstructionOrder.report();
        assertNotEquals(0, report.size());
    }

    @Test
    public void endOfProject() {
        String endOfProject = iConstructionOrder.endOfProject();
        assertNotEquals(null, endOfProject);
    }

}

package com.proof.sura.Builders.auxiliaryTest;

import com.proof.sura.Builders.models.SaveConstructionRequestDTO;

public class AuxiliaryConstructionRequestTest {
    public static SaveConstructionRequestDTO getConstructionRequestDTO() {
        return SaveConstructionRequestDTO.builder()
                .constructionTypeId(3)
                .coordinate("N 06 23 54 W 75 23 45")
                .build();
    }
}
